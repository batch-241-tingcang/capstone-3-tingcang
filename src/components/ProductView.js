import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

//function to view product after clicking the "Details" button
export default function ProductView() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [product, setProduct] = useState(null);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState("");
  const [quantity, setQuantity] = useState(1);

  // will retrieve the details of the products from our database to be displayed in the "ProductView" page
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProduct(data);

        setImage(data.imageURL);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, []);

  //Function to add products to cart
  const addToCart = async () => {
    const cartItems = JSON.parse(localStorage.getItem("cart"));

    if (cartItems?.some((productItem) => productItem._id === product._id)) {
    } else {
      localStorage.setItem(
        "cart",
        JSON.stringify((cartItems || []).concat(product))
      );

      //sweetAlert2 success notif after adding the product to cart
      const Toast = Swal.mixin({
        toast: true,
        position: "top-right",
        iconColor: "white",
        customClass: {
          popup: "colored-toast",
        },
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
      });
      await Toast.fire({
        icon: "success",
        title: "Successfully ordered the item!",
      });
    }

    console.log(cartItems);
  };

  const createOrder = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        products: [{ productId, productName: name, quantity }],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Checkout",
            icon: "success",
            text: "Ordered Successfully!",
          });
        }
      });
  };

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Img src={image}></Card.Img>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <div className="mb-2">
                <Card.Text>Quantity</Card.Text>
                <Form.Control
                  value={quantity}
                  type="number"
                  placeholder="An Amazing T-Shirt"
                  onChange={(e) => setQuantity(e.target.value)}
                  min="1"
                  autoFocus
                />
              </div>
              <Button className="bg-secondary me-3" as={Link} to="/">
                Back
              </Button>
              <Button className="bg-primary" onClick={(e) => createOrder()}>
                Checkout
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
