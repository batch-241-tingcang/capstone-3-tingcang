import { useState, useContext } from "react";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import CSS from "../App.css";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" className="navbar">
      <Navbar.Brand as={Link} to="/" className="navbar-text">
        Mikea
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/" className="navbar-text">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/products" className="navbar-text">
            Products
          </Nav.Link>

          {user.id !== null ? (
            <Nav.Link as={NavLink} to="/logout" className="navbar-text">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
            </>
          )}
          <Nav.Link as={NavLink} to="/orders" className="navbar-text">
            Orders
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
