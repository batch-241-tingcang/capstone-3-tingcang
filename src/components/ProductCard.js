import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Card, Button } from "react-bootstrap";

export default function ProductCard(product) {
  const { name, description, price, _id } = product;
  const [productCard, setProductCard] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
      method: "GET",
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProductCard(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <Row className="mt-3 mb-3">
      {productCard.map((productCards, index) => (
        <Col xs={12} md={4} key={index}>
          <Card className="productCard p-3">
            <Card.Body>
              {/* <Card.Image
                src={productHighlight.url}
                alt={productHighlight.description}
              /> */}
              <Card.Title>{productCards.name}</Card.Title>
              <Card.Text>{productCards.price}</Card.Text>
              <Button
                className="bg-primary"
                as={Link}
                to={`/products/${productCards._id}`}
              >
                Details
              </Button>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  );
}
