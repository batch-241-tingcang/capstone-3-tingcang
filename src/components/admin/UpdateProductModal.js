import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

export default function UpdateProductModal({
  show,
  setShow,
  fetchAllProducts,
  selectedProduct,
}) {
  console.log(selectedProduct);
  const [productName, setProductName] = useState(selectedProduct.name);
  const [productDescription, setProductDescription] = useState(
    selectedProduct.description
  );
  const [productPrice, setProductPrice] = useState(selectedProduct.price);
  const [productImageURL, setProductImageURL] = useState(
    selectedProduct.imageURL
  );
  const [productIsActive, setProductIsActive] = useState(
    selectedProduct.isActive
  );

  const handleClose = () => {
    setShow(false);
  };

  const onProductUpdate = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/${selectedProduct._id}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        price: productPrice,
        imageURL: productImageURL,
        isActive: productIsActive,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          fetchAllProducts();
          handleClose();
        }
      });
  };

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                value={productName}
                type="text"
                placeholder="An Amazing T-Shirt"
                onChange={(e) => setProductName(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={productDescription}
                onChange={(e) => setProductDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="PHP 0"
                min="0"
                value={productPrice}
                onChange={(e) => setProductPrice(e.target.valueAsNumber)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="productImageURL">
              <Form.Label>Image</Form.Label>
              <Form.Control
                value={productImageURL}
                type="link"
                placeholder="Image URL here"
                onChange={(e) => setProductImageURL(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Check
              type="switch"
              id="productIsActive"
              label="Product is Available"
              onChange={(e) => setProductIsActive(e.target.checked)}
              checked={productIsActive}
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={(e) => onProductUpdate(e)}>
            Update Product
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
