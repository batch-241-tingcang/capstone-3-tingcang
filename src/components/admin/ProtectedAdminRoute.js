import { useEffect } from "react";
import { Navigate } from "react-router-dom";

export default function ProtectedRoute({
  isAdmin,
  redirectPath = "/",
  children,
}) {
  useEffect(() => {
    if (localStorage.getItem("token")) {
      fetch(`${process.env.REACT_APP_API_URL}/users/my-profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          // User is logged in
          if (data.isActive) {
            return children;
            // User is logged out
          } else {
            return <Navigate to={redirectPath} replace />;
          }
        });
    }
  }, [isAdmin]);
}
