import "./App.css";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import CSS from "./App.css";

import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Products from "./pages/Products";
import AdminDashboard from "./pages/AdminDashboard";

import { Container } from "react-bootstrap";
import ProductView from "./components/ProductView";
import ShowAllOrders from "./pages/ShowAllOrders";
import ShowOrders from "./pages/ShowOrders";

export default function App() {
  console.log(process.env.REACT_APP_API_URL);
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      fetch(`${process.env.REACT_APP_API_URL}/users/my-profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          // User is logged in
          if (data.email) {
            setUser(data);
            // User is logged out
          } else {
            setUser({
              id: null,
              isAdmin: null,
            });
          }
        });
    }
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/admin/dashboard" element={<AdminDashboard />} />
              <Route path="/admin/orders" element={<ShowAllOrders />} />
              <Route path="/orders" element={<ShowOrders />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/*" element={<Error />} />
              <Route path="/" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
