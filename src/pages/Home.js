import Banner from "../components/Banner";
import ProductCard from "../components/ProductCard";

export default function Home() {
  const data = {
    title: "Mikea",
    content: "Transform Your Home into a Haven of Timeless Style",
    destination: "/",
    label: "Shop Now",
  };

  return (
    <>
      <Banner data={data} />
      <ProductCard />
    </>
  );
}
