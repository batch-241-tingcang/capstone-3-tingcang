import { useState, useEffect, useContext } from "react";
import { Table, Button, Form, Modal } from "react-bootstrap";
import AddProductModal from "../components/admin/AddProductModal";
import UpdateProductModal from "../components/admin/UpdateProductModal";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function AdminDashboard() {
  const { user, setUser } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  // Add new product modal
  const [productAddShow, setProductAddShow] = useState(false);
  // Update product modal
  const [productUpdateShow, setProductUpdateShow] = useState(false);
  // Store selected product for updating the product
  const [selectedProduct, setSelectedProduct] = useState({
    _id: "",
    name: "",
    description: "",
    price: 0,
    imageURL: "",
    isActive: true,
  });

  // Fetch all products
  const fetchAllProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  };

  // Toggle Availability for each product on Admin Dashboard
  const toggleAvailability = (e, product) => {
    e.preventDefault();
    fetch(
      `${process.env.REACT_APP_API_URL}/products/${product._id}/toggleAvailability`,
      {
        method: "PATCH",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          isActive: !product.isActive,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          fetchAllProducts();
        }
      });
  };

  // Function for showing the selected product when updating the product
  const onUpdateProductShow = (e, product) => {
    setProductUpdateShow(true);
    setSelectedProduct(product);
  };

  useEffect(() => {
    fetchAllProducts();
  }, []);

  if (user?.isAdmin === null) {
    return;
  }

  return user?.isAdmin ? (
    <>
      <AddProductModal
        show={productAddShow}
        setShow={setProductAddShow}
        fetchAllProducts={fetchAllProducts}
      />
      <UpdateProductModal
        key={selectedProduct._id}
        show={productUpdateShow}
        setShow={setProductUpdateShow}
        fetchAllProducts={fetchAllProducts}
        selectedProduct={selectedProduct}
      />
      <div className="text-center py-5">
        <h3>Admin Dashboard</h3>
        <Button
          variant="primary"
          className="me-3"
          onClick={() => setProductAddShow(true)}
        >
          Add New Product
        </Button>{" "}
        {/* <Button variant="success">Show Users Orders</Button>{" "} */}
      </div>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Image URL</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => {
            return (
              <tr key={product._id}>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.imageURL}</td>
                <td>
                  {product.isActive === true ? "Available" : "Unavailable"}
                </td>

                <td>
                  <Button
                    variant="primary"
                    className="mb-2"
                    onClick={(e) => onUpdateProductShow(e, product)}
                  >
                    Update
                  </Button>{" "}
                  <Button
                    variant={product.isActive === true ? "danger" : "success"}
                    onClick={(e) => toggleAvailability(e, product)}
                  >
                    {product.isActive === true ? "Disable" : "Enable"}
                  </Button>{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  ) : (
    <Navigate to="/" />
  );
}
