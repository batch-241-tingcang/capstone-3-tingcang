import { useState, useEffect, useContext } from "react";
import { Table, Button, Form, Modal, Accordion } from "react-bootstrap";
import AddProductModal from "../components/admin/AddProductModal";
import UpdateProductModal from "../components/admin/UpdateProductModal";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function ShowOrders(e) {
  const { user, setUser } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  const fetchUserOrders = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data?.length > 0) {
          setOrders(data);
        }
      });
  };

  useEffect(() => {
    fetchUserOrders();
  }, []);

  return (
    <>
      <div className="text-center py-5">
        <h3>Your Orders</h3>
      </div>

      <Accordion defaultActiveKey="0">
        {orders.map((order, i) => (
          <>
            <Accordion.Item eventKey={i}>
              <Accordion.Header>
                <div
                  className="d-flex justify-content-between pe-3"
                  style={{ width: "100%" }}
                >
                  <div>
                    <div>Order Number: {order._id}</div>
                    <small>{new Date(order.purchasedOn).toString()}</small>
                  </div>
                  <div>
                    <strong>Php {order.totalAmount}</strong>
                  </div>
                </div>
              </Accordion.Header>
              <Accordion.Body>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Quantity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {order.products.map((product) => {
                      return (
                        <tr key={product._id}>
                          <td>{product.productName}</td>
                          <td>{product.quantity}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </Accordion.Body>
            </Accordion.Item>
          </>
        ))}
      </Accordion>

      {/* <Table striped bordered hover>
        <thead>
          <tr>
            <th>Receipt ID</th>
            <th>Description</th>
            <th>Price</th>
            <th>Image URL</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => {
            return (
              <tr key={product._id}>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.imageURL}</td>
                <td>
                  {product.isActive === true ? "Available" : "Unavailable"}
                </td>

                <td>
                  <Button
                    variant="primary"
                    className="mb-2"
                    onClick={(e) => onUpdateProductShow(e, product)}
                  >
                    Update
                  </Button>{" "}
                  <Button
                    variant={product.isActive === true ? "danger" : "success"}
                    onClick={(e) => toggleAvailability(e, product)}
                  >
                    {product.isActive === true ? "Disable" : "Enable"}
                  </Button>{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table> */}
    </>
  );
}
