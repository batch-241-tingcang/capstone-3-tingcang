import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  //Function to simulate user registration
  function registerUser(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.success) {
          Swal.fire({
            title: "Registration successful",
            icon: "success",
            text: "Welcome to Mikea",
          });
          setEmail("");
          setPassword1("");
          setPassword2("");
          setIsActive(false);
          navigate("/login");
        } else {
          Swal.fire({
            title: data.message,
            icon: "error",
            text: "Please try again",
          });
        }
      });
  }

  // Checking if email exists in the database AND then register the user
  // const checkEmail = (e) => {
  //   e.preventDefault();
  //   fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
  //     method: "POST",
  //     headers: { "Content-Type": "application/json" },
  //     body: JSON.stringify({ email, password: password1 }),
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       if (data === true) {
  //         Swal.fire({
  //           title: "Duplicate email found",
  //           icon: "error",
  //           text: "Please provide a different email!",
  //         });
  //       } else {
  //         registerUser();
  //       }
  //     });
  // };

  // Validation to enable submit button when all fields are populated and both password match
  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/active" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      <br />

      {/*conditionally render submit button based on "isActive" state*/}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
