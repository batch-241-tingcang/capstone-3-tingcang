import { useState, useEffect, useContext } from "react";
import { Table, Button, Form, Modal } from "react-bootstrap";
import AddProductModal from "../components/admin/AddProductModal";
import UpdateProductModal from "../components/admin/UpdateProductModal";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function ShowUsersOrders(e) {
  const { user, setUser } = useContext(UserContext);

  // Fetch all orders
  // const fetchAllOrders = () => {
  //   fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
  //     headers: {
  //       "Content-type": "application/json",
  //       Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     },
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       setProducts(data);
  //     });
  // };

  // useEffect(() => {
  //   fetchAllOrders();
  // }, []);

  if (user?.isAdmin === null) {
    return;
  }

  return user.isAdmin === false ? (
    <>
      {/* <Table striped bordered hover>
        <thead>
          <tr>
            <th>{product.name}</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => {
            return (
              <>
                <tr key={product._id}>
                  <td>{product.description}</td>
                </tr>
                <tr>
                  <Button
                    variant="primary"
                    className="mb-2"
                    onClick={(e) => onUpdateProductShow(e, product)}
                  >
                    Checkout
                  </Button>
                </tr>
              </>
            );
          })}
        </tbody>
      </Table> */}
    </>
  ) : (
    <Navigate to="/" />
  );
}
